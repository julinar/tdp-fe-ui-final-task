import './assets/css/style.css';

// assets image
import banner from './assets/img/architect.jpg';
import house2 from './assets/img/house2.jpg';
import house3 from './assets/img/house3.jpg';
import house4 from './assets/img/house4.jpg';
import house5 from './assets/img/house5.jpg';
import map from './assets/img/map.jpg';
import team1 from './assets/img/team1.jpg';
import team2 from './assets/img/team2.jpg';
import team3 from './assets/img/team3.jpg';
import team4 from './assets/img/team4.jpg';

function App() {
  return (
    <div className="App">

      {/* Navbar (sit on top) */}
      <div className='navbar-container'>
        <div className='navbar'>
          <a href="/#" className='nav-item-left'><b>EDTS</b> TDP Batch #2</a>
          <a href="/#contact" className='nav-item-right'>Contact</a>
          <a href="/#about" className='nav-item-right'>About</a>
          <a href="/#projects" className='nav-item-right'>Projects</a>
        </div>
      </div>

      {/* Header */}
      <header>
        <img src={banner} alt="Architecture" width={1500} height={800} />
        <div className='title-image'>
          <h1>
              <b>EDTS</b><span>Architects</span>
          </h1>
        </div>
      </header>

      {/* Page content */}
      <div className="container">

        {/* Project Section */}
        <div id="projects">
          <h3>Projects</h3>
        </div>
        
        <div className='projects-content'>
          <div className='projects-item'>
            <div >Summer House</div>
            <img src={house2} alt="House" />
          </div>
          <div className='projects-item'>
            <div>Brick House</div>
            <img src={house3} alt="House" />
          </div>
          <div className='projects-item'>
            <div>Renovated</div>
            <img src={house4} alt="House" />
          </div>
          <div className='projects-item'>
            <div >Barn House</div>
            <img src={house5} alt="House" />
          </div>
          <div className='projects-item'>
            <div>Summer House</div>
            <img src={house3} alt="House" />
          </div>
          <div className='projects-item'>
            <div>Brick House</div>
            <img src={house2} alt="House" />
          </div>
          <div className='projects-item'>
            <div>Renovated</div>
            <img src={house5} alt="House" />
          </div>
          <div className='projects-item'>
            <div>Barn House</div>
            <img src={house4} alt="House" />
          </div>
        </div>

        {/* About Section */}
        <div id='about'>
          <h3>About</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Excepteur sint
            occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur
            adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat.
          </p>
        </div>
        <div className='about-content'>
          <div className='about-item'>
            <img src={team1} alt="Jane" />
            <h3>Jane Doe</h3>
            <p><span>CEO &amp; Founder</span></p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
          <div className='about-item'>
            <img src={team2} alt="John" />
            <h3>John Doe</h3>
            <p><span>Architect</span></p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
          <div className='about-item'>
            <img src={team3} alt="Mike" />
            <h3>Mike Ross</h3>
            <p><span>Architect</span></p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
          <div className='about-item'>
            <img src={team4} alt="Dan" />
            <h3>Dan Star</h3>
            <p><span>Architect</span></p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
        </div>

        {/* Contact Section */}
        <div id="contact">
          <h3>Contact</h3>
          <p>Lets get in touch and talk about your next project.</p>
          <form>
            <div>
              <input type="text" id="name" name="name" placeholder="Name" />
            </div>
            <div>
              <input type="email" id="email" name="email" placeholder="Email" />
            </div>
            <div>
              <input type="text" id="subject" name="subject" placeholder="Subject" />
            </div>
            <div>
              <input type="text" id="comment" name="comment" placeholder="Comment" />
            </div>
            <input type="submit" value="SEND MESSAGE"></input>
          </form>
        </div>
        
        {/* Image of location/map */}
        <div className='content-maps'>
          <img src={map} alt="maps" />
        </div>

        <div className='contact-list'>
          <h3>Contact List</h3>

          <table>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Country</th>
            </tr>
            <tr>
              <td>Alfreds Futterkiste</td>
              <td>alfreds@gmail.com</td>
              <td>Germany</td>
            </tr>
            <tr>
              <td>Reza</td>
              <td>reza@gmail.com</td>
              <td>Indonesia</td>
            </tr>
            <tr>
              <td>Ismail</td>
              <td>ismail@gmail.com</td>
              <td>Turkey</td>
            </tr>
            <tr>
              <td>Holand</td>
              <td>holand@gmail.com</td>
              <td>Belanda</td>
            </tr>
          </table>

        </div>

        {/* End page content */}
      </div>

      {/* Footer */}
      <footer>
        <p>Copyright 2022</p>
      </footer>
      
    </div>
  );
}

export default App;
